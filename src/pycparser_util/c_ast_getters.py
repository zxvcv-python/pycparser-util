from pycparser import c_ast


class ABCGetter(c_ast.NodeVisitor):
    def __init__(self):
        self.items = []

class FuncDeclGetter(ABCGetter):
    def visit_FuncDecl(self, node):
        self.items.append(node)

class TypedefGetter(ABCGetter):
    def visit_Typedef(self, node):
        self.items.append(node)

class TypeDeclGetter(ABCGetter):
    def visit_TypeDecl(self, node):
        self.items.append(node)

class StructGetter(ABCGetter):
    def visit_Struct(self, node):
        self.items.append(node)

class EnumGetter(ABCGetter):
    def visit_Enum(self, node):
        self.items.append(node)
