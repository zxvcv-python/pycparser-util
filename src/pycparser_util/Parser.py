import copy
import shutil

from pathlib import Path
from pycparser import c_parser, preprocess_file, parse_file
from typing import List, Optional, Dict

from zxvcv.util.Path import here


class Parser():
    def __init__(self):
        pass

    def parse_file(self, file:Path,
                   includes:List[Path] = [],
                   defines:Dict[str, Optional[str]] = {},
                   compiler_params:List[str] = [],
                   handle_gnu_specific_symbols:bool = False):
        tmp_dir = here(__file__)/"tmp"
        try:
            tmp_file = tmp_dir/file.name
            try:
                shutil.rmtree(tmp_dir)
            except FileNotFoundError:
                pass
            tmp_dir.mkdir(parents=True)
            shutil.copyfile(file, tmp_file)

            if handle_gnu_specific_symbols:
                with tmp_file.open("r") as f:
                    file_content = f.read()

                includes = copy.deepcopy(includes)
                includes.append(here(__file__)/"c_utils")
                file_content = "#include \"gnu_specific_symbols.h\"\n" + file_content

                with tmp_file.open("w") as f:
                    f.write(file_content)

            preprocessed_content = preprocess_file(
                filename=tmp_file,
                cpp_path="gcc",
                cpp_args=["-E"] \
                    + [f"-I{path}" for path in includes] \
                    + [f"-D{d}={v}" if v else f"-D{d}" for d, v in defines.items()]
                    + compiler_params
            )
            parser = c_parser.CParser()
            ast = parser.parse(preprocessed_content, tmp_file)

            return ast
        finally:
            try:
                shutil.rmtree(tmp_dir)
            except FileNotFoundError:
                pass
