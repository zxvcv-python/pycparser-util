import re

from pathlib import Path
from typing import Optional, List, Dict

from .Parser import Parser
from .CRegex import CRegex
from .c_objects import CObject
from .c_ast_getters import ABCGetter


class CParserUtil():
    @staticmethod
    def find_items(getter:ABCGetter,
                   output_type:CObject,
                   files:List[Path],
                   includes:List[Path] = [],
                   defines:Dict[str, Optional[str]] = {},
                   compiler_params:List[str] = [],
                   skip_included:bool = False,
                   handle_gnu_specific_symbols:bool = False) -> dict:
        for file in files:
            ast = Parser().parse_file(
                file=file,
                includes=includes,
                defines=defines,
                compiler_params=compiler_params,
                handle_gnu_specific_symbols=handle_gnu_specific_symbols
            )

            fd_get = getter()
            fd_get.visit(ast)

            with open(file, "r") as file:
                file_content = file.read()

            # remove non doc multiline (dont change total number of lines in)
            def process_match(match):
                return "\n" * (len(match.group().splitlines()) - 1)
            file_content = re.sub(CRegex._MULTILINE_NOT_DOC_COMMENT_SEQUENCE, process_match, file_content)

            # remove single line comments
            # TODO[PP]: what if single comment line will be inside doc comment - for now it prohibited
            file_content = re.sub(CRegex._SINGLELINE_COMMENT, "", file_content)

            for fdecl in fd_get.items:
                item = output_type(ast=fdecl, file=file, file_content=file_content)
                if skip_included and \
                   not item.search_for_definiton_in_file(file=file,
                                                         file_content=file_content,
                                                         attribute_sensitive=True):
                    continue
                yield item
