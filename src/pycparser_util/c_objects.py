import re

from pathlib import Path
from pycparser import c_ast
from abc import ABC, abstractmethod

from .CRegex import CRegex


class CObject(ABC):
    def __init__(self, ast:c_ast.FuncDecl, file:Path, file_content:str = None):
        self.ast = ast
        self.file = file
        self.file_content = file_content

    def _get_respective_class(self, object):
        return eval(f"C{object.__class__.__name__.split('.')[-1]}")

    @abstractmethod
    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        pass

    def search_for_definiton_in_file(self, file:Path, file_content:str = None, attribute_sensitive:bool = False):
        if not file_content:
            with open(file, "r") as f:
                file_content = f.read()

        regex = self.get_regex(attribute_sensitive=attribute_sensitive)
        match = re.search(
            self.get_regex(attribute_sensitive=attribute_sensitive).get_compiled(with_doc=True),
            file_content
        )

        return match if match else None

    def get_cdoc(self, attribute_sensitive:bool = False):
        match = self.search_for_definiton_in_file(
            file=self.file,
            file_content=self.file_content,
            attribute_sensitive=attribute_sensitive
        )

        if not match:
            # TODO[PP]: print debug info
            return None

        cdoc_match = re.search(
            CRegex.get_regex_multiline_comment().get_compiled(),
            match.group()
        )

        return cdoc_match.group() if cdoc_match else None

class CFuncDecl(CObject):
    def __init__(self, ast:c_ast.FuncDecl, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).name

    @property
    def type_str(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).type_str

    @property
    def params(self):
        for param in self.ast.args.params:
            clazz = self._get_respective_class(param)
            yield clazz(param, self.file, self.file_content)

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        # TODO[PP]: handle comments in the middle of the function declaration
        # TODO[PP]: handle preprocessor statements

        params = []
        for param in self.params:
            params.append(param.get_regex(attribute_sensitive=attribute_sensitive))
            # params.append(CRegex.handle_whitespaces_in_pointer_type(param.type_str))
            # params.append(param.name)
            params.append(",")
        params = params[:-1]

        regex = [
            CRegex.handle_whitespaces_in_pointer_type(self.type_str),
            CRegex.get_regex_attribute(optional=True) if attribute_sensitive else None,
            self.name,
            r"\(",
            *params,
            r"\)",
            CRegex.get_regex_attribute(optional=True) if attribute_sensitive else None,
            r";"
        ]
        return CRegex(regex)

class CDecl(CObject):
    def __init__(self, ast:c_ast.Decl, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.name

    @property
    def type_str(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).type_str

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        clazz = self._get_respective_class(self.ast.type)
        instance = clazz(self.ast.type, self.file, self.file_content)
        return CRegex([
            instance.get_regex(attribute_sensitive=attribute_sensitive)#, # declaration type
            # self.name#, # declaration name
            # r";"
        ])

class CPtrDecl(CObject):
    def __init__(self, ast:c_ast.PtrDecl, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).name

    @property
    def type(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content)

    @property
    def type_str(self):
        return f"{self.type.type_str}*"

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        clazz = self._get_respective_class(self.ast.type)
        instance = clazz(self.ast.type, self.file, self.file_content)

        if isinstance(instance, CFuncDecl):
            clazz = self._get_respective_class(instance.ast.args)
            instance_args = clazz(instance.ast.args, self.file, self.file_content)

            clazz = self._get_respective_class(instance.ast.type)
            instance_type = clazz(instance.ast.type, self.file, self.file_content)

            regex = [
                instance_type.get_regex(attribute_sensitive=attribute_sensitive),
                r"\(", r"\*", instance_type.name, r"\)",
                r"\(", instance_args.get_regex(attribute_sensitive=attribute_sensitive), r"\)"
            ]
            # TODO[PP]: this is a workarround until more elegant solution would be found
            del regex[0].regex[1]
        else:
            temp = [r"\*"]
            while isinstance(instance, CPtrDecl):
                clazz = self._get_respective_class(instance.ast.type)
                instance = clazz(instance.ast.type, self.file, self.file_content)
                temp.append(r"\*")
            regex = [instance.get_regex(attribute_sensitive=attribute_sensitive)]
            # TODO[PP]: this is a workarround until more elegant solution would be found
            for i in temp:
                regex[0].regex.insert(1, i)

        return CRegex(regex)

class CTypeDecl(CObject):
    def __init__(self, ast:c_ast.TypeDecl, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.declname

    @property
    def type(self):
        if isinstance(self.ast.type, str):
            return " ".join(self.ast.type.names)

        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content)

    @property
    def type_str(self):
        if isinstance(self.ast.type, str):
            return self.ast.type.names[0]
        if isinstance(self.ast.type, c_ast.Struct):
            return self.type.name
        if isinstance(self.ast.type, c_ast.Enum):
            return ""
        return self.type.type_str

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        clazz = self._get_respective_class(self.ast.type)
        instance = clazz(self.ast.type, self.file, self.file_content)

        return CRegex([
            instance.get_regex(attribute_sensitive=attribute_sensitive),
            self.name
        ])

class CStruct(CObject):
    def __init__(self, ast:c_ast.Struct, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.name

    @property
    def members(self):
        if not self.ast.decls:
            return []
        for param in self.ast.decls:
            clazz = self._get_respective_class(param)
            yield clazz(param, self.file, self.file_content)

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        if len(list(self.members)) > 0:
            regex = [
                r"struct",
                self.name,
                r"{"
            ]
            for member in self.members:
                regex += [
                    member.get_regex(attribute_sensitive=attribute_sensitive),
                    r";"
                ]
            regex += [
                r"}",
                "[^\/]*?", # optional typedef name of the structure
                r";"
            ]
        else:
            regex = [
                r"(struct)?",
                self.name
            ]
        return CRegex(regex)

class CArrayDecl(CObject):
    def __init__(self, ast:c_ast.ArrayDecl, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def type_str(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).type_str

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        return CRegex()

class CIdentifierType(CObject):
    def __init__(self, ast:c_ast.IdentifierType, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return " ".join(self.ast.names)

    @property
    def type_str(self):
        return self.name

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        return CRegex([self.name])

class CTypedef(CObject):
    def __init__(self, ast:c_ast.Typedef, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.name

    @property
    def type(self):
        clazz = self._get_respective_class(self.ast.type)
        return  clazz(self.ast.type, self.file, self.file_content)

    @property
    def type_str(self):
        return self.type.type_str

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        return CRegex([
            r"typedef",
            self.type.type.get_regex(attribute_sensitive=attribute_sensitive)
        ])

class CEnum(CObject):
    def __init__(self, ast:c_ast.Enum, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.name

    @property
    def values(self):
        clazz = self._get_respective_class(self.ast.values)
        return clazz(self.ast.values, self.file, self.file_content).enumerators

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        values = []
        for value in self.values:
            values.append(value.get_regex(attribute_sensitive=attribute_sensitive))
            values.append(",")
        values = values[:-1]

        return CRegex([
            r"enum",
            self.name,
            r"{",
            *values,
            r"}",
            "[^\/]*?", # optional typedef name of the enum
            r";"
        ])

class CEnumeratorList(CObject):
    def __init__(self, ast:c_ast.EnumeratorList, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def enumerators(self):
        for enumerator in self.ast.enumerators:
            clazz = self._get_respective_class(enumerator)
            yield clazz(enumerator, self.file, self.file_content)

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        regex = []
        for enumerator in self.enumerators:
            regex.append(enumerator.get_regex(attribute_sensitive=attribute_sensitive))
            regex.append(r",")
        return CRegex(regex)

class CEnumerator(CObject):
    def __init__(self, ast:c_ast.Enumerator, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.name

    @property
    def type_str(self):
        clazz = self._get_respective_class(self.ast.value)
        instance = clazz(self.ast.value, self.file, self.file_content)
        return instance.type_str

    @property
    def value(self):
        clazz = self._get_respective_class(self.ast.value)
        instance = clazz(self.ast.value, self.file, self.file_content)
        return instance.value

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        return CRegex([
            self.name,
            r"(=",
            self.value,
            r")?"
        ])

class CConstant(CObject):
    def __init__(self, ast:c_ast.Constant, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def type_str(self):
        return self.ast.type

    @property
    def value(self):
        return self.ast.value

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        return CRegex()

class CParamList(CObject):
    def __init__(self, ast:c_ast.Typename, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def params(self):
        for param in self.ast.params:
            clazz = self._get_respective_class(param)
            yield clazz(param, self.file, self.file_content)

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        regex = []
        for param in self.params:
            regex.append(param.get_regex(attribute_sensitive=attribute_sensitive))
            regex.append(",")
        return CRegex(regex[:-1])

class CTypename(CObject):
    def __init__(self, ast:c_ast.Typename, file:Path, file_content:None):
        super().__init__(ast, file, file_content)

    @property
    def name(self):
        return self.ast.name if self.ast.name else ""

    @property
    def type(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).type

    @property
    def type_str(self):
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).type_str

    def get_regex(self, attribute_sensitive:bool = False) -> CRegex:
        clazz = self._get_respective_class(self.ast.type)
        return clazz(self.ast.type, self.file, self.file_content).get_regex(
            attribute_sensitive=attribute_sensitive
        )
