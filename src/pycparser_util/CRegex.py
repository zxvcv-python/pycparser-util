import re

from typing import List


class CRegex():
    # match any sequence of whitespace characters
    _WHITESPACE_CHAR_SEQUENCE = r"\s*"

    # match multiline comment sequence
    _MULTILINE_COMMENT_SEQUENCE = r"\/\*[^\/]*?\*\/"
    # _MULTILINE_COMMENT_SEQUENCE = r"\/\*\*?.*[^\*\/]*?\*\/"

    # match multiline doc comment sequence
    _MULTILINE_DOC_COMMENT_SEQUENCE = r"\/\*\*.*[^\*\/]*?\*\/"

    # match multiline not doc comment sequence
    _MULTILINE_NOT_DOC_COMMENT_SEQUENCE = r"\/\*[^\*].*[^\*\/]*?\*\/"

    # match multiline comment sequence
    _SINGLELINE_COMMENT = r"\/\/.*"

    # match commetns and whitespace sequence
    _COMMENTS_AND_WHITESPACE_SEQUENCE = "".join([
        # _WHITESPACE_CHAR_SEQUENCE,
        r"(", _MULTILINE_COMMENT_SEQUENCE, r")?",
        _WHITESPACE_CHAR_SEQUENCE
        # r"(", _SINGLELINE_COMMENT, r")?"
        # _WHITESPACE_CHAR_SEQUENCE
    ])

    @classmethod
    def get_regex_multiline_comment(cls):
        return CRegex([cls._MULTILINE_COMMENT_SEQUENCE])

    @classmethod
    def get_regex_attribute(cls, optional:bool = False):
        return CRegex(list(filter(None, [
            r"(" if optional else None,
            r"__attribute__(.*)",
            r")?" if optional else None
        ])))

    @classmethod
    def handle_whitespaces_in_pointer_type(cls, type_name:str) -> str:
        return type_name.replace(r"*", cls._WHITESPACE_CHAR_SEQUENCE + r"\*")


    def __init__(self, regex:List[str]= []):
        self.regex = list(filter(None, regex))

    @staticmethod
    def _resolve(cregex:"CRegex") -> List[str]:
        regex_str = []
        for r in cregex.regex:
            if type(r) == CRegex:
                regex_str = regex_str + CRegex._resolve(r)
                # regex_str.append(CRegex._resolve(r))
            elif type(r) == str:
                regex_str.append(r)
            else:
                raise ValueError("Could not resolve regex.")
        return regex_str

    def get_compiled(self, with_doc:bool = False):
        return re.compile(self.get_stringify(with_doc=with_doc))

    def get_stringify(self, with_doc:bool = False):
        gluer = "".join([
            CRegex._WHITESPACE_CHAR_SEQUENCE,
            CRegex._COMMENTS_AND_WHITESPACE_SEQUENCE

        ])
        regex = CRegex._resolve(cregex=self)
        regex = gluer.join(regex)
        if with_doc:
            regex = CRegex._COMMENTS_AND_WHITESPACE_SEQUENCE +\
                    CRegex._WHITESPACE_CHAR_SEQUENCE +\
                    regex
        return regex
