Changelog
=========

0.2.0b1 (2023-02-15)
--------------------
- Python support for version 3.10 3.11.
- Update project configurations and informations.

0.1.10 (2022-11-06)
-------------------
- Removing commnents before searching for items in file with CParserUtil.find_items().
- Add predefined regex to CRegex: _MULTILINE_DOC_COMMENT_SEQUENCE, _MULTILINE_NOT_DOC_COMMENT_SEQUENCE.
- Rename and update predefined regex to CRegex: _SINGLELINE_COMMENT_AT_EOL -> _SINGLELINE_COMMENT.

0.1.9 (2022-11-05)
------------------
- CRegex restructurization and updates.
- CObject.get_regex() method parameters update.

0.1.8 (2022-11-01)
------------------
- Add parameter handle_gnu_specific_symbols to CParserUtil.find_items().
- Add parameter attribute_sensitive to CObject.search_for_definiton_in_file().
- Add method CRegex.get_regex_attribute().
- Parser now creates temporary file before preprocessing.
- Rework getting regex from pfunction definition parameter list.
- Add gnu_specific_symbols.h file included during file parsing to hangle gnu attributes.

0.1.7 (2022-10-30)
------------------
- Improvements/Fixes for get_regex() methods.

0.1.6 (2022-10-29)
------------------
- Add parameter skip_included to CParserUtil.find_items().
- Add EnumGetter.
- Add method CObject.search_for_definiton_in_file().
- Add type_str() funcitons in childs for CObject classes.
- type() will now return object instead of str type (use type_str instead).

0.1.5 (2022-10-29)
------------------
- get_regex() methods moved from CRegex to c_objects classes.
- updates for implementation get_regex in c_object classes
- new c_ibject classes: CArrayDecl, CIdentifierType

0.1.4 (2022-10-27)
------------------
- Remove: ConverterAstToStr (functionality moved to c_object getters).
- c_objects.py contains now all CObjects classes.
- Add getters: FuncDeclGetter, TypedefGetter, TypeDeclGetter, StructGetter.
- __handle_whitespaces_in_pointer_type as method in CRegex class.
- get_regex_multiline_comment as method in CRegex class.
- CParserUtil generic find_items instead of find_function_ for every cobject.

0.1.3 (2022-10-25)
------------------
- Move: CGetters.py(CGetter) -> CParserUtil.py(CParserUtil)
- Move: CGetters.py(FuncDeclGetter) -> c_ast_getters.py(FuncDeclGetter)
- Move: c_str.py(FunDecl) -> c_objects/CFunDecl.py(CFunDecl)

0.1.2 (2022-10-23)
------------------
- Add CRegex class.

0.1.1 (2022-10-22)
------------------
- Importing package fix (with name pycparser_util).

0.1.0 (2022-10-22)
------------------
- Initial commit.
