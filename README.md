# pycparser-util
Common used calsses and functions for forking with c files.

# Important informations
CParserUtil.find_items() will remove all multiline comments that are not doc string (/** */) and single line comments before processing file

Using single line comment in docstring may resolve unexpected behaviour.

# Build
Build all with single command run:
```
tox
```

Build wheel for specific python:
```
tox -e py37
tox -e py38
tox -e py39
tox -e py310
tox -e py311
```

# Publish
> require tokens for pypi and testpypi in ~/.pypirc file

### publish on pypi
```
tox -e publish
```

### publish on testpypi
```
tox -e publish-test
```
